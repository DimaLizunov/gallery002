package ru.startandroid.gallery002.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import ru.startandroid.gallery002.gallery_manager.FragmentImageView;


public class FullScreenImageAdapter extends FragmentPagerAdapter {

    private ArrayList<String> images;

    public FullScreenImageAdapter(FragmentManager fm, ArrayList<String> images) {
        super(fm);
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentImageView.newInstance(position, images);
    }

}