package ru.startandroid.gallery002.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ru.startandroid.gallery002.R;
import ru.startandroid.gallery002.gallery_manager.ShownImages;


public class MainActivityAdapter extends BaseAdapter {

    public ArrayList<String> images;

    private ShownImages shownImages = new ShownImages();

    public MainActivityAdapter(Context context) {
        images = shownImages.getAllShownImagesPath(context.getApplicationContext());
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void addImage(String url) {
        images.add(url);
        notifyDataSetChanged();
    }

    public int getCount() {
        return images.size();
    }

    public Object getItem(int position) {
        return images.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        ImageView picturesView;
        if (convertView == null) {
            picturesView = new ImageView(parent.getContext());
            picturesView.setLayoutParams(new GridView.LayoutParams(
                    GridView.LayoutParams.MATCH_PARENT, GridView.LayoutParams.MATCH_PARENT));
        } else {
            picturesView = (ImageView) convertView;
        }

        Glide.with(parent.getContext())
                .load(images.get(position))
                .placeholder(R.drawable.ic_launcher2)
                .fitCenter()
                .into(picturesView);

        return picturesView;
    }


}
