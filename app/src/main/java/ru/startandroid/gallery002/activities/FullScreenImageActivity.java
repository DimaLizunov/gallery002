package ru.startandroid.gallery002.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import ru.startandroid.gallery002.R;
import ru.startandroid.gallery002.adapters.FullScreenImageAdapter;
import ru.startandroid.gallery002.gallery_manager.DBHelper;
import ru.startandroid.gallery002.gallery_manager.ShownImages;


public class FullScreenImageActivity extends AppCompatActivity {

    public static final String KEY_IMAGES = "IMAGES";

    public ArrayList<String> stringArrayListExtra;
    public DBHelper dbHelper;
    public SQLiteDatabase database;
    public String path;
    public int position;
    public double loc_long;
    public double loc_lat;

    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscrn);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DBHelper(this);
        database = dbHelper.getReadableDatabase();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        stringArrayListExtra = getIntent().getStringArrayListExtra(KEY_IMAGES);

        position = getIntent().getIntExtra("position", 0);

        FullScreenImageAdapter adapter = new FullScreenImageAdapter(getSupportFragmentManager(),
                stringArrayListExtra);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {

            public void transformPage(View page, float position) {
                page.setRotationY(position * -30);
            }

        });
    }


    private void scanFile(String path) {
        MediaScannerConnection.scanFile(FullScreenImageActivity.this,
                new String[]{path}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("TAG", "Finished scanning " + path);
                    }
                });
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.gps_item).setVisible(false);
        path = stringArrayListExtra.get(viewPager.getCurrentItem());
        for (int checkIdPath = 0; checkIdPath < imagesFromDatabase().size(); checkIdPath++) {
            if (path.equals(imagesFromDatabase().get(checkIdPath))) {
                menu.findItem(R.id.gps_item).setVisible(true);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_del, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        path = stringArrayListExtra.get(viewPager.getCurrentItem());

        ShownImages shownImages = new ShownImages();
        Intent intent = new Intent(FullScreenImageActivity.this, MainActivity.class);

        switch (item.getItemId()) {

            case android.R.id.home:
                this.finish();
                return true;

            case R.id.gps_item:
                gpsItemToMap();
                break;

            case R.id.del_item:
                shownImages.delImage(this, path);
                scanFile(path);
                startActivity(intent);
                Toast.makeText(FullScreenImageActivity.this, R.string.delToast,
                        Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void gpsItemToMap() {
        String selection = "title = ?";
        String[] selectionArgs = new String[]{ path };
        Cursor cursor = database.query("images", null, selection, selectionArgs, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    loc_long = Double.parseDouble(cursor.getString(cursor.
                            getColumnIndex(DBHelper.KEY_LONGITUDE)));
                    loc_lat = Double.parseDouble(cursor.getString(cursor.
                            getColumnIndex(DBHelper.KEY_LATITUDE)));
                    Intent mapIntent = new Intent(FullScreenImageActivity.this, MapsActivity.class);
                    mapIntent.putExtra("loc_long", loc_long);
                    mapIntent.putExtra("loc_lat", loc_lat);
                    startActivity(mapIntent);
                } while (cursor.moveToNext());
            } else {
                Toast.makeText(FullScreenImageActivity.this, R.string.empty_loc,
                        Toast.LENGTH_SHORT).show();
            }
            cursor.close();
        }
    }


    public ArrayList<String> imagesFromDatabase() {
        String[] selectionImages = new String[]{ "title" };
        ArrayList<String> listOfImagesDB = new ArrayList<>();
        Cursor c = database.query("images", selectionImages, null, null, null, null, null);
        while (c.moveToNext()) {
            String pathOfImageDB = c.getString(c.getColumnIndex(DBHelper.KEY_URL));
            listOfImagesDB.add(pathOfImageDB);
        }
        c.close();
        return (listOfImagesDB);
    }


    @Override
    protected void onDestroy() {
        database.close();
        super.onDestroy();
    }
}


