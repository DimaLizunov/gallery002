package ru.startandroid.gallery002.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import ru.startandroid.gallery002.R;
import ru.startandroid.gallery002.gallery_manager.DBHelper;

public class NoteActivity extends AppCompatActivity {

    public SQLiteDatabase database;
    public String mediaFile;
    public DBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        mediaFile = getIntent().getStringExtra("mediaFile");
        ImageView imageView = (ImageView) findViewById(R.id.fullScrnImgNote);
        Button btnNote = (Button) findViewById(R.id.btnSaveNote);

        btnNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNoteForImage();
            }
        });

        Glide.with(this)
                .load(mediaFile)
                .placeholder(R.drawable.ic_launcher2)
                .fitCenter()
                .into(imageView);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_skip, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(NoteActivity.this, MainActivity.class));
        return super.onOptionsItemSelected(item);
    }

    public void setNoteForImage() {
        dbHelper = new DBHelper(this);
        ContentValues contentValues = new ContentValues();
        database = dbHelper.getWritableDatabase();
        EditText editText = (EditText) findViewById(R.id.noteText);

        contentValues.put(DBHelper.KEY_NOTE, editText.getText().toString());

        String selection = "title = ?";
        String[] selectionArgs = new String[]{ mediaFile };
        database.update(DBHelper.TABLE_IMAGES, contentValues, selection, selectionArgs);
        startActivity(new Intent(NoteActivity.this, MainActivity.class));
        Toast.makeText(NoteActivity.this, "Your note added to database", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        database.close();

    }
}
