package ru.startandroid.gallery002.activities;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.startandroid.gallery002.R;
import ru.startandroid.gallery002.adapters.MainActivityAdapter;
import ru.startandroid.gallery002.gallery_manager.DBHelper;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static ru.startandroid.gallery002.activities.FullScreenImageActivity.KEY_IMAGES;


public class MainActivity extends AppCompatActivity implements LocationListener {

    private static final int PERMS_REQUEST_CODE_EXTR = 123;
    private static final int PERMS_REQUEST_CODE_CAMERA = 124;
    private static final int PERMS_REQUEST_CODE_LOC = 125;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final String IMAGE_DIRECTORY_NAME = "My Gallery";

    public Uri fileUri;
    public File mediaFile;
    public String loc;
    public MainActivityAdapter adapter;
    public DBHelper dbHelper;
    public SQLiteDatabase database;
    public ContentValues contentValues;
    public ImageView imageView;

    protected LocationManager locationManager;


    private File getOutputMediaFile(int type) {
        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());

        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    private void scanFile(String path) {

        MediaScannerConnection.scanFile(MainActivity.this,
                new String[]{path}, null,
                new MediaScannerConnection.OnScanCompletedListener() {

                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("TAG", "Finished scanning " + path);
                    }
                });
    }


    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }


    public void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }


    public void galleryClick() {
        GridView gallery = (GridView) findViewById(R.id.galleryGridView);

        adapter = new MainActivityAdapter(this);

        gallery.setAdapter(adapter);
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, FullScreenImageActivity.class);
                intent.putStringArrayListExtra(KEY_IMAGES, adapter.getImages());
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (hasCameraPermission()) {
            captureImage();
        } else {
            requestCameraPerms();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        imageView = (ImageView) findViewById(R.id.imgView);

        dbHelper = new DBHelper(this);
        database = dbHelper.getWritableDatabase();
        contentValues = new ContentValues();

        if (hasExtStrPermission()) {
            galleryClick();
        } else {
            requestExtStrPerms();
        }
    }

    @Override
    protected void onStart() {
        if (mediaFile != null) {
            scanFile(mediaFile.getAbsolutePath());
        }
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                adapter.addImage(mediaFile.toString());
                Intent intent = new Intent(MainActivity.this, NoteActivity.class);
                intent.putExtra("mediaFile", mediaFile.toString());
                startActivity(intent);
                if (hasLocationPermission()) {
                    showLastLocation();
                } else {
                    requestLocationPerms();
                }
            }
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
        }
    }


    private boolean hasExtStrPermission() {
        int res;

        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;

        res = checkCallingOrSelfPermission(permission);
        return res == PackageManager.PERMISSION_GRANTED;
    }


    private boolean hasCameraPermission() {
        int res;

        String permission = Manifest.permission.CAMERA;

        res = checkCallingOrSelfPermission(permission);
        return res == PackageManager.PERMISSION_GRANTED;
    }


    private boolean hasLocationPermission() {
        int res;

        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        for (String perm : permissions) {
            res = checkCallingOrSelfPermission(perm);
            if (!(res == PackageManager.PERMISSION_GRANTED)) {
                return false;
            }
        }
        return true;
    }


    private void requestExtStrPerms() {
        String[] permission = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permission, PERMS_REQUEST_CODE_EXTR);
        }
    }


    private void requestCameraPerms() {
        String[] permission = new String[]{Manifest.permission.CAMERA};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permission, PERMS_REQUEST_CODE_CAMERA);
        }
    }


    private void requestLocationPerms() {
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, PERMS_REQUEST_CODE_LOC);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == PERMS_REQUEST_CODE_EXTR) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                galleryClick();
            } else {
                Toast.makeText(this, "Storage Permissions denied.", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == PERMS_REQUEST_CODE_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                captureImage();
            } else {
                Toast.makeText(this, "Camera Permission denied.", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == PERMS_REQUEST_CODE_LOC) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                showLastLocation();
            } else {
                Toast.makeText(this, "Location Permission denied.", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }


    @Override
    public void onProviderDisabled(String provider) {

    }


    public void showLastLocation() {
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (location != null) {
            loc = "Latitude: " + location.getLatitude() + " Longitude: " +
                    location.getLongitude();
            Toast.makeText(MainActivity.this, loc, Toast.LENGTH_LONG).show();

            database = dbHelper.getWritableDatabase();
            contentValues = new ContentValues();

            contentValues.put(DBHelper.KEY_URL, mediaFile.toString());
            contentValues.put(DBHelper.KEY_LATITUDE, location.getLatitude());
            contentValues.put(DBHelper.KEY_LONGITUDE, location.getLongitude());

            database.insert(DBHelper.TABLE_IMAGES, null, contentValues);

            dbHelper.close();

        } else {
            Toast.makeText(MainActivity.this, R.string.null_loc,
                    Toast.LENGTH_LONG).show();
        }
    }
}