package ru.startandroid.gallery002.gallery_manager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ru.startandroid.gallery002.R;


public class FragmentImageView extends Fragment {

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";

    public ImageView imageView;
    public int position;
    public ArrayList<String> images;


    public static FragmentImageView newInstance(int position, ArrayList<String> images) {
        FragmentImageView pageFragment = new FragmentImageView();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, position);
        arguments.putStringArrayList("myList", new ArrayList<>(images));
        pageFragment.setArguments(arguments);
        return pageFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        images = getArguments().getStringArrayList("myList");
        position = getArguments().getInt(ARGUMENT_PAGE_NUMBER);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        imageView = (ImageView) view.findViewById(R.id.imgView);
        setHasOptionsMenu(true);

        ((ViewGroup) imageView.getParent()).removeView(imageView);

        Glide.with(container.getContext())
                .load(images.get(position))
                .placeholder(R.drawable.ic_launcher2)
                .into(imageView);
        return imageView;
    }

}
