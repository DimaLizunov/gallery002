package ru.startandroid.gallery002.gallery_manager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;


public class ShownImages {

    public ArrayList<String> getAllShownImagesPath(Context context) {
        Uri uri;
        Cursor cursor;
        int column_index_data;
        ArrayList<String> listOfAllImages = new ArrayList<>();
        String absolutePathOfImage;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        cursor = context.getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            listOfAllImages.add(absolutePathOfImage);
        }
        cursor.close();

        return listOfAllImages;
    }

    public void delImage(Context context, String path) {
        context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.Media.DATA + "='" + path + "'", null);
    }

    public ArrayList<String> getAllShownImagesLongitude(Context context) {
        Cursor cursor;
        int column_index_data;
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        ArrayList<String> listOfAllLongitude = new ArrayList<>();
        Double locationOfImage;
        String[] selection = new String[] { "longitude" };

        cursor = database.query("images", selection, null, null, null, null, null);

        column_index_data = cursor.getColumnIndexOrThrow(DBHelper.KEY_LONGITUDE);

        while (cursor.moveToNext()) {
            locationOfImage = cursor.getDouble(column_index_data);
            listOfAllLongitude.add(locationOfImage.toString());
        }
        cursor.close();

        return listOfAllLongitude;
    }

    public ArrayList<String> getAllShownImagesLatitude(Context context) {
        Cursor cursor;
        int column_index_data;
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        ArrayList<String> listOfAllLatitude = new ArrayList<>();
        Double locationOfImage;
        String[] selection = new String[] {"latitude"};

        cursor = database.query("images", selection, null, null, null, null, null);

        column_index_data = cursor.getColumnIndexOrThrow(DBHelper.KEY_LATITUDE);

        while (cursor.moveToNext()) {
            locationOfImage = cursor.getDouble(column_index_data);
            listOfAllLatitude.add(locationOfImage.toString());
        }
        cursor.close();

        return listOfAllLatitude;
    }
}


