package ru.startandroid.gallery002.gallery_manager;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "imageDB";
    public static final String TABLE_IMAGES = "images";

    public static final String KEY_ID = "_id";
    public static final String KEY_URL = "title";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_NOTE = "note";

    public static final String DATABASE_CREATE_IMAGES = "create table " + TABLE_IMAGES + "(" +
            KEY_ID + " integer primary key," + KEY_URL + " text," + KEY_LATITUDE + " double," +
            KEY_LONGITUDE + " double" + ")";
    private static final String DATABASE_ALTER_IMAGES_1 = "ALTER TABLE " + TABLE_IMAGES +
            " ADD COLUMN " + KEY_NOTE + " string;";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_IMAGES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            db.execSQL(DATABASE_ALTER_IMAGES_1);
        }
    }

}
